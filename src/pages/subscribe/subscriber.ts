import { Component } from '@angular/core';
import { ModalController, NavController, LoadingController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { SuccessPage } from '../modal/success';
import { ErrorPage } from '../modal/error';


@Component({
  selector: 'page-subscriber',
  templateUrl: 'subscriber.html',
  providers: [SuccessPage, ErrorPage]
})

export class SubscriberPage {
  private contact_message : FormGroup;
  status_code;
  modal_status;
  leparams;

  constructor(public navCtrl: NavController, private formBuilder: FormBuilder, public http: Http, public modalCtrl: ModalController, public loading: LoadingController ) {
    this.contact_message = this.formBuilder.group({
      number: ['', Validators.required]

    });
  }
  ContactForm(){

    let loader = this.loading.create({
      content: "Por favor espere...",
    });

    loader.present().then(() => {
      console.log(this.contact_message.value);

      let headers = new Headers({ 'Content-Type': 'application/json; charset=UTF-8' });

      let options = new RequestOptions({ headers: headers });


      let postParams =
      {
        contact_message: {
          "phone": this.contact_message.value.number,
        }
      }

      console.log(postParams);

      this.http.post("http://losvecinosserie.com/api/subscribers.json", postParams, options)
        .subscribe(data => {
          this.status_code = data.json().status;

          if(this.status_code == "201"){
            this.modal_status = SuccessPage;
            this.openModal(this.modal_status);
            this.contact_message.reset();
          }
          else{
            this.modal_status = ErrorPage;
            this.openModal(this.modal_status);
          }

         }, error => {
          console.log(error);// Error getting the data
        });



      loader.dismiss();
    });

  }

  openModal(status) {
    let modal = this.modalCtrl.create(status);
    modal.present();
  }

}
