import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-chapter',
  templateUrl: 'chapter.html'
})

export class ChapterPage {
  item;
  constructor(public navCtrl: NavController, public params: NavParams) {
    this.item = params.data.item;
  }
}
