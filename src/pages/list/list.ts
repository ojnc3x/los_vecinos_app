import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  posts: any;
  rows: any;
  snetwork: boolean = false;

  constructor(public nav: NavController, public http: Http, private network: Network, public loading: LoadingController) {
    this.doRefresh(0);
    if(this.network.type==='none'){
      this.snetwork = true;
    }

    let loader = this.loading.create({
      content: "Por favor espere...",
    });

    loader.present().then(() => {
      this.http.get('http://losvecinosserie,com/api/casts.json')
        .map(res =>
          res.json().map((item) => {
             item = item;
             return item;
           })
        )
        .subscribe(
          (response) => {
            this.rows = response;
            loader.dismiss();
          },
          (error) => {
            console.log(error);
            loader.dismiss();
          }
        );


      });
  }

  doRefresh(refresher){
    let loader = this.loading.create({
      content: "Por favor espere...",
    });

    loader.present().then(() => {
      this.http.get('http://losvecinosserie.com/api/casts.json')
      //this.http.get('http://reencuentro.co/api/v1/missings.json')
        .map(res =>
          res.json().map((item) => {
             item = item;
             return item;
           })
        )
        .subscribe(
          (response) => {
            this.rows = response;
            loader.dismiss();
          },
          (error) => {
            console.log(error);
            loader.dismiss();
          }
        );
      });
      if(refresher != 0)
        refresher.complete();
  };

}
