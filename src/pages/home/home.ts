import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { ChapterPage } from '../chapters/chapter';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

import 'rxjs/add/operator/map';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [ChapterPage]
})

export class HomePage {

  posts: any;
  rows: any;
  snetwork: boolean = false;

  constructor(public nav: NavController, public http: Http, private network: Network, public loading: LoadingController, private youtube: YoutubeVideoPlayer) {
    this.doRefresh(0);
    if(this.network.type==='none'){
      this.snetwork = true;
    }

    let loader = this.loading.create({
      content: "Por favor espere...",
    });

    loader.present().then(() => {
      //this.http.get('http://losvecinosserie/api/v1/last_chapter.json')
      this.http.get('http://losvecinosserie.com/api/episodes/last_episode.json')
        .map(res =>
          res.json().map((item) => {
             item = item;
             return item;
           })
        )
        .subscribe(
          (response) => {
            this.rows = response;
            loader.dismiss();
          },
          (error) => {
            console.log(error);
            loader.dismiss();
          }
        );


      });
  }

  doRefresh(refresher){
    let loader = this.loading.create({
      content: "Por favor espere...",
    });

    loader.present().then(() => {
      //this.http.get('http://losvecinosserie/api/last_chapter.json')
      this.http.get('http://losvecinosserie.com/api/episodes/last_episode.json')
        .map(res =>
          res.json().map((item) => {
             item = item;
             return item;
           })
        )
        .subscribe(
          (response) => {
            this.rows = response;
            loader.dismiss();
          },
          (error) => {
            console.log(error);
            loader.dismiss();
          }
        );
      });
      if(refresher != 0)
        refresher.complete();
  };

  openNavDetailsPage(post) {
    this.nav.push(ChapterPage, { item: post });
  }

  openYoutubeVideo(post){
    this.youtube.openVideo(post.youtube_url_id);
  }

}
