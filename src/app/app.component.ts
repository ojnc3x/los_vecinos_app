import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ChaptersPage } from '../pages/chapters/chapters';
import { SubscriberPage } from '../pages/subscribe/subscriber';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any, id: string}>;





  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private youtube: YoutubeVideoPlayer) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Inicio', component: HomePage, id: "home" },
      { title: 'Episodios', component: ChaptersPage, id: "chapters" },
      { title: 'Suscribete', component: SubscriberPage, id: "subscribe" },
      { title: 'Elenco', component: ListPage, id: "cast" },
    ];

    //Ejemplo youtube ID
    //this.youtube.openVideo('myvideoid');

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
