import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';


import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ChaptersPage  } from '../pages/chapters/chapters';
import { ChapterPage } from '../pages/chapters/chapter';
import { SubscriberPage  } from '../pages/subscribe/subscriber';

import { SuccessPage } from '../pages/modal/success';
import { ErrorPage } from '../pages/modal/error';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

//import { CloudSettings, CloudModule } from '@ionic/cloud-angular';

/*const cloudSettings: CloudSettings = {
  'core': {
    'app_id': 'APP_ID'
  }
};*/

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    ChaptersPage,
    ChapterPage,
    SubscriberPage,
    ErrorPage,
    SuccessPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    //CloudModule.forRoot(cloudSettings),
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    ChaptersPage,
    ChapterPage,
    SubscriberPage,
    ErrorPage,
    SuccessPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    YoutubeVideoPlayer,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
